package inf101v22.trafficlight;

import inf101v22.trafficlight.TrafficLight.AlreadyStartedException;

public class App {
    
    public static void main(String[] args) throws AlreadyStartedException {

        TrafficLight trafficLight = new TrafficLight();
        trafficLight.turnOn();
        System.out.println(trafficLight);
        
        trafficLight.goToNextState();
        trafficLight.state = TrafficLight.State.HURRY;
        System.out.println(trafficLight);

        trafficLight.goToRed();
        System.out.println(trafficLight);
    }

    
}
